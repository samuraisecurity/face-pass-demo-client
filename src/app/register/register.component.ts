import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  isOpenImageSelect = true;
  parentData = '親のコンポーネントから渡す値だよ';
  selectedGlobalZodiac;
  takenPicture = false;

  // カメラのための設定
  @ViewChild('video') videoElm: ElementRef;
  @ViewChild('canvas') canvasElm: ElementRef;

  readonly medias: MediaStreamConstraints = {
    audio: false, video: {
      facingMode: 'user', // フロントカメラ指定
      // リアカメラ指定するときは以下に差し替える
      // facingMode: {
      //   exact : 'environment'
      // },
    }
  };

  // captureデータ
  captureData: string;

  constructor() {
  }

  ngOnInit() {
  }

  onReceiveEventFromChild($event) {
    const imageSelect = JSON.parse($event);
    console.log('子からのイベント：' + 'row:' + imageSelect.row + 'col:' + imageSelect.col);
    this.selectedGlobalZodiac = imageSelect;
    this.isOpenImageSelect = false;
  }

  backImageSelect() {
    this.isOpenImageSelect = true;
  }

  startCamera() {
    this.canvasElm.nativeElement.getContext('2d')
      .clearRect(0, 0, this.canvasElm.nativeElement.width, this.canvasElm.nativeElement.height);
    window.navigator.mediaDevices.getUserMedia(this.medias)
      .then(stream => this.videoElm.nativeElement.srcObject = stream)
      .catch(error => {
        console.error(error);
        alert(error);
      });
  }

  stopCamera(clearCanvas: boolean) {
    this.videoElm.nativeElement.pause();
    const track = this.videoElm.nativeElement.srcObject.getTracks()[0] as MediaStreamTrack;
    track.stop();
    this.videoElm.nativeElement.src = '';

    if (clearCanvas) {
      this.canvasElm.nativeElement.getContext('2d')
        .clearRect(0, 0, this.canvasElm.nativeElement.width, this.canvasElm.nativeElement.height);
      this.takenPicture = false;
    }
  }

  takePicture() {
    // 写真のサイズを決める
    const WIDTH = this.videoElm.nativeElement.clientWidth;
    const HEIGHT = this.videoElm.nativeElement.clientHeight;

    // canvasを用意する
    const ctx = this.canvasElm.nativeElement.getContext('2d') as CanvasRenderingContext2D;
    this.canvasElm.nativeElement.width = WIDTH;
    this.canvasElm.nativeElement.height = HEIGHT;

    // canvasの描画をしつつBase64データを取る
    this.captureData = this.canvasElm.nativeElement.toDataURL(ctx.drawImage(this.videoElm.nativeElement, 0, 0, WIDTH, HEIGHT));

    // cameraを停止
    this.canvasElm.nativeElement.toDataURL(this.captureData);
    this.stopCamera(false);
    this.takenPicture = true;
    return;
  }

  register() {
    // send server this.captureData & this.selectedGlobalZodiac
    console.log(this.captureData.substring(0, 100));
    console.log(JSON.stringify(this.selectedGlobalZodiac));
  }

}
