import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoiseGlobalZodiacComponent } from './choise-global-zodiac.component';

describe('ChoiseGlobalZodiacComponent', () => {
  let component: ChoiseGlobalZodiacComponent;
  let fixture: ComponentFixture<ChoiseGlobalZodiacComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChoiseGlobalZodiacComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoiseGlobalZodiacComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
