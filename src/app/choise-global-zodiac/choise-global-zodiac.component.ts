import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-choise-global-zodiac',
  templateUrl: './choise-global-zodiac.component.html',
  styleUrls: ['./choise-global-zodiac.component.css']
})
export class ChoiseGlobalZodiacComponent implements OnInit {

  // 親から子へ受け渡すのに必要
  @Input() parentData: string;

  // 子から親へ受け渡すのに必要
  @Output() event = new EventEmitter<string>();

  numbers = [
    [1, 2, 3],
    [1, 2, 3],
    [1, 2, 3],
    [1, 2, 3],
  ];

  constructor() {
  }

  ngOnInit() {
    console.log('親から渡されたデータだよ：' + this.parentData);
  }

  public selectImage(row, col) {
    console.log('click row:' + row.toString() + ', col:' + col.toString());
    const toParent = {
      row: row,
      col: col,
    }
    this.event.emit(JSON.stringify(toParent));
  }

}
