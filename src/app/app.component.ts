import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  isOpenRegistar = false;
  isOpenAuthentication = false;

  openRegister() {
    this.isOpenRegistar = !this.isOpenRegistar;
  }

  openAuthentication() {
    this.isOpenAuthentication = !this.isOpenAuthentication;
  }

}
