# 簡単使い方

## 環境構築
node のバージョン8.9をインストール

合わせてnpmをインストール

任意のフォルダで

```Bash
npm install -g @angular/cli
```

当プロジェクトの配下で

```Bash
npm install
```

## 起動方法

```Bash
npm start
```

コンソールにURLが表示されます。 default localhost:4200

## 実装方法
Angularはコンポーネントベースのため

コンポーネントの追加

（パーツを親のコンポーネントに配置していくイメージ）

トップページ（app.component.html）では、以下を切り替え

  register (登録コンポーネント)

  authentication (照合コンポーネント)

コンポーネントの追加は以下のコマンドを叩くことで作られる

```Bash
ng generate component コンポーネント名(headerとか)
```

追加したものは、コンポーネントのフォルダ

（headerという名前で追加した場合は、headerフォルダが作成され
tsにセレクタ「app-header」の記載があるので、
app-headerというタグを入れるとheaderフォルダのhtml(css, ts等含む)が表示される）

## 変数をHTMLにバインドする。
htmlとtsは双方向バインドが有効です。

html : {{text}} ts: text = 'face-pass-demo';

のように定義すると

どちらかを変更するともう片方のあたいも変更されます。

他にも親のコンポーネントから子、子のコンポーネントから親のサンプルをサンプルのために実装している箇所があります。

# FacePassDemo

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
